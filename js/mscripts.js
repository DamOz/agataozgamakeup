jQuery(function($)
{
	$.scrollTo(0);

	$('#link1').click(function() { $.scrollTo($('#frontPage'), 500); });
	$('#link2').click(function() { $.scrollTo($('#oMnie'), 500); });
	$('#link3').click(function() { $.scrollTo($('#portfolio'), 500); });
	$('#link4').click(function() { $.scrollTo($('#dlaCiebie'), 500); });
	$('#link5').click(function() { $.scrollTo($('#contact'), 500); });
	$('.ao-hero-scrollup').click(function() { $.scrollTo($('body'), 1000); });

}
);

$(window).scroll(function()
{
	if($(this).scrollTop()>300) $('.ao-hero-scrollup').fadeIn();
	else $('.ao-hero-scrollup').fadeOut();
}
);